import requests

page = requests.get("http://w3.ufsm.br/ru/index.php/cardapio").text
table = page[page.find("<tbody>")+7:page.find("</tbody>")]
table = table.replace('&nbsp;','').replace('\r\n','')
for a,b in (('&aacute;','á'),('&atilde;','ã'),('&ccedil;','ç'), ('&eacute;','é'),('&iacute;','í'),('&ecirc;','ê')):
    table = table.replace(a,b)
while True:
    a = table.find('<')
    b = table.find('>')+1
    tag = table[a:b]
    if a is -1 or b is -1:
        break
    elif tag not in ('</tr>','</td>','</th>'):
        table = table.replace(tag,'')
    else:
        table = table.replace(tag,'[t'+tag[3]+']')
table = table.split('[tr]')[:-1]
#table[0] = table[0].split('[th]')[:-1]
for i in range(0,len(table)):
    table[i] = table[i].split('[td]')[:-1]

dias = []
for d in table[0]:
    dia = d[d.find('(')+1:d.find(')')]
    dias.append({'dia': dia,
                 'id': int(dia[6:]+dia[3:5]+dia[:2]),
                 'dia_s': d.replace('('+dia+')','')})

campos = ['dia','carne','vegan','verdura','legume','acompanhamento','arroz','arroz(2)',
          'feijão','suco','sobremesa']
for c in range(1,len(table)):
    for i in range(6):
        dias[i][campos[c]] = table[c][i]

'''
for d in dias:
    print("%14s\t%s"%("id",d['id']))
    for c in campos:
        print("%14s\t%s"%(c,d[c]))
    print("")
'''
print (dias)
