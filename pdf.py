import PyPDF2, os
from login import *

def getIDA_MGA(session, data):
    p = session.get("https://portal.ufsm.br/aluno/relatorio/indiceDesempenhoAcademico/index.html")
    f = open("/tmp/"+data['matricula']+".pdf","wb")
    f.write(p.content)
    f.close()
    pdf = PyPDF2.PdfFileReader("/tmp/"+data['matricula']+".pdf").getPage(0).extractText()
    os.system('rm /tmp/'+data['matricula']+'.pdf')
    pos_ida = pdf.find("Acadêmico:") + 10
    pos_mga = pdf.find("Fórmula de Cálculo do Índice de Desempenho Acadêmico") + 52
    data = {'ida':float(pdf[pos_ida : pdf.find("O",pos_ida)].replace(".","").replace(",",".")),
            'mga':float(pdf[pos_mga : pdf.find("M",pos_mga)].replace(",","."))}
    return data

if __name__ == "__main__":
    if len(sys.argv) == 3:
        d = {'matricula':sys.argv[1], 'senha':sys.argv[2]}
        s = criarSession(d)
        r = getIDA_MGA(s,d)
        print (r)
    else:
        print("Uso: "+sys.argv[0]+" matricula senha")
