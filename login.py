#!/usr/bin/python3
# -*- coding: UTF-8 -*-
__author__ = '648trindade'
import requests, sys

def login(session, dados):
    try:
        url = 'https://portal.ufsm.br/usuario/j_security_check'
        page = session.post(url, data={'j_username':dados['matricula'], 'j_password':dados['senha']})
    except requests.exceptions.ConnectionError:
        return "ConnectionError"
    else:
        if page.url == 'https://portal.ufsm.br/usuario/restrita/index.html':
            content = str(page.text)
            ini = content.find("user") + 11
            dados['nome'] = content[ini : content.find(" <", ini)]
            return "LoginSuccessfull"
        else:
            return "LoginError"

def getCurso(session, dados):
    page = session.get("https://portal.ufsm.br/aluno/index.html")
    content = page.text
    ini = content.find("list-v") + 14
    string = content[ini : content.find("</li>", ini)]
    dados['curso'] = string[string.find(" - ") + 3:]

def criarSession(dados):
    if dados['matricula'].isdigit():
        session = requests.Session()
        res = login(session, dados)
        if res == "LoginSuccessfull":
            print("[ OK ] Login sucedido para "+dados['nome'])
            dados['login_successfull'] = True
            return session
        elif res == "LoginError":
            print("[ERRO] Matrícula ou senha incorretos")
        elif res == "ConnectionError":
            print("[ERRO] Falha temporária na resolução do nome. Cheque sua conexão com a internet.")
    else:
        print("[ERRO] Matrícula deve consistir somente de números")
    dados['login_successfull'] = False
    return None

if __name__ == "__main__":
    if len(sys.argv) == 1:
        dados = {'matricula':'201411819', 'senha':'senhadorafa'}
        session = criarSession(dados)
        if dados['login_successfull']:
            getCurso(session, dados)
            print(dados)
            #getSaldo(session, dados)
            #print(dados['saldo'])
            sys.exit(0)
    else:
        print("Uso: "+sys.argv[0]+" matricula senha")
    sys.exit(1)
