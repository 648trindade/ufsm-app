#!/usr/bin/python3
# -*- coding: UTF-8 -*-
from login import *
from time import localtime
__author__ = '648trindade'

def getSaldo(session, dados):
    page = session.get("https://portal.ufsm.br/ru/usuario/extratoSimplificado.html")
    content = page.text
    ini = content.find("saldoUsuario") + 14
    strSaldo = content[ini : content.find("</label>", ini)]
    dados['saldo'] = float(strSaldo[strSaldo.find("R$") + 3 : strSaldo.find(",") + 2].replace(',','.'))

def getRefeicoes(session, dados, mes=None, ano=None):
    if mes is None:
        mes = str(localtime().tm_mon)
    if ano is None:
        ano = str(localtime().tm_year)
    page = session.post("https://portal.ufsm.br/ru/usuario/extratoSimplificado.html",data={"mes":mes, "ano":ano})
    content = page.text
    tbody = content[content.find("<tbody>")+7 : content.find("</tbody>")]
    for i in ("\n", "</tr>", "</td>", "</span>", " class=\"align-right\"",
              "<span class=\"bold debito\">", "<span class=\"bold credito\">"):
        tbody = tbody.replace(i,'')
    linhas = tbody.split("<tr>")[1:]
    ref = []
    for i in range(len(linhas)):
        ref.append(linhas[i].split("<td>")[1:])
        for j in range(len(ref[i])):
            ref[i][j] = ref[i][j].strip()
    return ref

if __name__ == '__main__':
    if len(sys.argv) == 5:
        dados = {'matricula':sys.argv[1], 'senha':sys.argv[2]}
        mes, ano = sys.argv[3:]
        session = criarSession(dados)
        if session is not None:
            ref = getRefeicoes(session, dados, mes, ano)
            for r in ref:
                print("\t".join(r))
            sys.exit(0)
    else:
        print("Uso: "+sys.argv[0]+" matricula senha mes ano")
    sys.exit(1)
